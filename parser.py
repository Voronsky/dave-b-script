import argparse
import string
import random
import csv

'''
    This tool is for Dave B
    We are expecting a file of the following format

    F.name --> name.F
'''


def writeCSV(fmtList, output):
    with open(output, 'w', newline='') as csvfile:
        writer = csv.writer(csvfile, delimiter=' ', quotechar=',', quoting=csv.QUOTE_MINIMAL)
        for user in fmtList:
            writer.writerow([user[0], user[1], user[2]]) 
        print(f'CSV generated: {output}')


def parseFile(filename):
    fmt_list = [] # global List of 3-tuples, (name.a, name A, password)
    with open(filename, 'r') as a_file:
        for line in a_file:
            oldName = line.strip() # Strip the white spaces at begin and end
            nameFmt = oldName.split('.') # Reverse it
            name = nameFmt[1]+'.'+nameFmt[0]
            lastname_firstletter = name.replace('.', ' ')
            password = string.digits + string.ascii_uppercase + string.ascii_lowercase
            randomDigitStr = ''.join(random.choice(password) for i in range(16)) # generate pass
            entry = (name, lastname_firstletter, randomDigitStr)
            fmt_list.append(entry)

    return fmt_list

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", type=str, help="File of the names to parse into csv")
    parser.add_argument("-o", "--output", default="users.csv", type=str, help="CSV output filename")
    args = parser.parse_args()
    if args.filename:
        entries = parseFile(args.filename)
        writeCSV(entries, args.output)
